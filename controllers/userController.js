const User = require("../models/User");

module.exports.getUsers = () => {
    return User.find({}).then((result) => result);
};

module.exports.postUser = (requestBody) => {
    console.log(requestBody);
    return new User(requestBody).save().then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            return result;
        }
    });
};
