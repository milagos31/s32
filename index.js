const express = require("express");
const app = express();
const PORT = 3001;
const userRoute = require("./routes/userRoute");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//Import routes
app.use("/users", userRoute);

const mongoose = require("mongoose");
const URI = "mongodb+srv://admin:admin1234@zuitt-bootcamp.inrmr.mongodb.net/s32?retryWrites=true&w=majority";
mongoose.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.once("open", () => console.log("connected to MongoDB Atlas"));

app.listen(PORT, () => {
    console.log("server is running at port: ", PORT);
});
