const router = require("express").Router();
const userController = require("../controllers/userController");

router.get("/", (req, res) => {
    userController.getUsers().then((result) => res.send(result));
});

router.post("/", (req, res) => {
    userController.postUser(req.body).then((result) => {
        res.send(result);
    });
});

module.exports = router;
